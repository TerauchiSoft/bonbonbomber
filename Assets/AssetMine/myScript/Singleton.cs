﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace TSSoft
{
    /// <summary>
    /// シングルトンクラス
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        /// <summary>
        /// インスタンスへのアクセス
        /// </summary>
        public static T Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                instance = FindObjectOfType(typeof(T)) as T;
                return instance;
            }
        }
        protected static T instance;

        protected virtual void Start()
        {
            if (instance != null && instance != this)
            {
                if (this.gameObject.transform.parent.gameObject != null)
                {
                    GameObject.Destroy(this.gameObject.transform.parent.gameObject);
                }
                else
                {
                    GameObject.Destroy(this.gameObject);
                }
                return;
            }
            else
            {
                instance = this as T;
                if (this.gameObject.transform.parent != null)
                {
                    DontDestroyOnLoad(this.gameObject.transform.parent.gameObject);
                }
                else
                {
                    DontDestroyOnLoad(this.gameObject);
                }
            }
        }
    }

    /// <summary>
    /// シングルトンクラス
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T> where T : class, new()
    {
        /// <summary>
        /// インスタンスへのアクセス
        /// </summary>
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new T();
                }
                return instance;
            }
        }
        protected static T instance;
    }
}
