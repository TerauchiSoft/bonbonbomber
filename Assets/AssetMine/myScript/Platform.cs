﻿namespace TSSoft
{
    /// <summary>
    /// プラットフォームごとに文字列を返す。
    /// </summary>
    public class Platform : Singleton<Platform>
    {
#if UNITY_STANDALONE_WIN
        private const string PLATFORM = "Windows";
#elif UNITY_WEBGL
        private const string PLATFORM = "WebGL";
#else
        private const string PLATFORM = "Unknown";
#endif
        /// <summary>
        /// プラットフォーム名を返す
        /// </summary>
        /// <returns></returns>
        public string GetPlatform()
        {
            return PLATFORM;
        }
    }
}