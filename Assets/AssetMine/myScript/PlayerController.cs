﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TSSoft
{
    /// <summary>
    /// コントローラー装置。入力をキャッシュに保存している。押しっぱなしでの連続入力に対応。
    /// 高頻度の使用の場合は各クラス内にPlayerController plycnt;を宣言しています。
    /// </summary>
    public class PlayerController : SingletonMonoBehaviour<PlayerController>
    {
        /*-----------------------------------
         * 									*
         * 				入力許可				*
         * 									*
        -----------------------------------*/
        /// <summary>
        /// 入力許可
        /// </summary>
        public bool isInputAllow;
        /// <summary>
        /// キャンセルキー メニュー開閉ボタン使用可能
        /// </summary>
        public bool isMenuAllow;
        /// <summary>
        /// 移動ボタン使用可能
        /// </summary>
        public bool isMoveAllow;
        /// <summary>
        /// 文字列表示中
        /// 外部のメソッドで変化する。
        /// </summary>
        public bool isPlayString;
        /// <summary>
        /// イベントを調べられるかどうか。
        /// </summary>
        public bool isEventAllow;
        /// <summary>
        /// trueでキーダウンで読み込み
        /// falseで押しっぱなしで読み込み
        /// </summary>
        public bool isKeyDownMode = true;
        /// <summary>
        /// ディレイ
        /// </summary>
        [SerializeField]
        private int delay = 0;
        /// <summary>
        /// 軸ディレイ
        /// </summary>
        [SerializeField]
        private int axisdelay = 0;

        [SerializeField]
        private readonly int delayflame = 3;
        [SerializeField]
        private readonly int axisdelayflame = 6;
        [SerializeField]
        private bool isFirstAxis = true;
        [SerializeField]
        private bool isRepeat;
        [SerializeField]
        private readonly int axisinitflame = 10;

        public static int FirsrKeyWait { get { return Instance.axisinitflame; } }
        public static int RepeatKeyWait { get { return Instance.axisdelayflame; } }
        public static bool IsRepeat { get { return Instance.isRepeat; } }

        // ジョイスティックの入力バッファ
        public bool[] joy_buttons_buf = new bool[3];
        public bool joy_buttons_leftPad;
        public bool joy_buttons_rightPad;
        public Vector2 joy_Axis_buf;

        // -------------------- 入力待ちモード -------------------------

        public bool _isInputWaiting;
        public System.Action _waitAction;
        public static void GotoInputWaitMode(System.Action act)
        {
            PlayerController.Instance.ResetInput(10);
            PlayerController.Instance._isInputWaiting = true;
            PlayerController.Instance._waitAction = act;
        }

        public void CheckWaitingAndRun()
        {
            this.ButtonInputSet();
            this.AxisInputSet();
            if (this.IsButtonAndAxisIsNoZero)
            {
                this._isInputWaiting = false;
                this._waitAction.Invoke();
                this._waitAction = null;
                PlayerController.Instance.ResetInput(10);
                return;
            }
            return;
        }

        // -------------------------------------------------------------

        private void ButtonInputSet()
        {
            for (int i = 0; i < joy_buttons_buf.Length; i++)
            {
                if (this.isKeyDownMode)
                    this.joy_buttons_buf[i] = Input.GetButtonDown("Fire" + (1 + i).ToString());
                else
                    this.joy_buttons_buf[i] = Input.GetButton("Fire" + (1 + i).ToString());
            }
            if (this.isKeyDownMode)
            {
                //joy_buttons_leftPad = Input.GetButtonDown("LeftPad");
                //joy_buttons_rightPad = Input.GetButtonDown("RightPad");
            }
            else
            {
                //joy_buttons_leftPad = Input.GetButton("LeftPad");
                //joy_buttons_rightPad = Input.GetButton("RightPad");
            }
        }

        private bool IsAxisZero()
        {
            var a = Input.GetAxis("Horizontal");
            var b = Input.GetAxis("Vertical");
            return (a == 0 && b == 0);
        }

        private bool IsButtonZero()
        {
            var a = Input.GetAxis("Horizontal");
            var b = Input.GetAxis("Vertical");
            return (a == 0 && b == 0);
        }

        private void AxisInputSet()
        {
            if (this.isKeyDownMode == true)
            {
                if (this.axisdelay > 0)
                {
                    this.joy_Axis_buf.x = 0.0f;
                    this.joy_Axis_buf.y = 0.0f;
                    return;
                }
            }
            AxisInputValGet();
        }

        private void AxisInputValGet()
        {
            this.joy_Axis_buf.x = Input.GetAxis("Horizontal");
            this.joy_Axis_buf.y = Input.GetAxis("Vertical");

            if (this.joy_Axis_buf.x == 0 && this.joy_Axis_buf.y == 0)
                return;

            if (this.isFirstAxis == true)
            {
                this.axisdelay = (int)(this.axisinitflame / 60.0f / Time.smoothDeltaTime);
                //Debug.Log(axisdelay);
                this.isFirstAxis = false;
            }
            else
            {
                this.isRepeat = true;
                this.axisdelay = (int)(this.axisdelayflame / 60.0f / Time.smoothDeltaTime);
            }
        }

        /// <summary>
        /// ボタンの読込方法
        /// </summary>
        /// <param name="b"></param>
        public void SetKeyDownMode(bool b)
        {
            this.isKeyDownMode = b;
        }

        /// <summary>
        /// 連続で入力の防止
        /// </summary>
        public void SetInputDelay()
        {
            this.delay = (int)(this.delayflame / 60.0f / Time.smoothDeltaTime);
            for (int i = 0; i < joy_buttons_buf.Length; i++)
                this.joy_buttons_buf[i] = false;
            this.joy_Axis_buf.x = 0.0f;
            this.joy_Axis_buf.y = 0.0f;
        }

        /// <summary>
        /// メニュー等連続入力禁止
        /// </summary>
        public void SetInputDelay(int dly)
        {
            this.delay = (int)(dly / 60.0f / Time.smoothDeltaTime);
        }


        public void ResetInput(int dly = -1)
        {
            if (dly == -1)
                dly = this.delayflame;

            this.SetInputDelay(dly);
            this.SetAxisDelay();

            for (int i = 0; i < this.joy_buttons_buf.Length; i++)
                this.joy_buttons_buf[i] = false;
            this.joy_Axis_buf.x = 0.0f;
            this.joy_Axis_buf.y = 0.0f;
        }

        /// <summary>
        /// 連続で入力の防止
        /// </summary>
        private void SetAxisDelay()
        {
            this.axisdelay = (int)(this.axisdelayflame / 60.0f / Time.smoothDeltaTime);
        }

        /// <summary>
        /// 動作全ロック
        /// </summary>
        public void ControlLock()
        {
            this.isMoveAllow = false;
            this.isMenuAllow = false;
        }

        public void SetAllow(bool a)
        {
            this.isInputAllow = a;
        }

        public bool isFire1
        {
            get { return (isInputAllow == false) ? false : this.joy_buttons_buf[0]; }
        }
        public bool isFire2
        {
            get { return (isInputAllow == false) ? false : this.joy_buttons_buf[1]; }
        }
        public bool isFire3
        {
            get { return (isInputAllow == false) ? false : this.joy_buttons_buf[2]; }
        }
        public float Horizontal
        {
            get { return (isInputAllow == false) ? 0.0f : this.joy_Axis_buf.x; }
        }
        public float Virtical
        {
            get { return (isInputAllow == false) ? 0.0f : joy_Axis_buf.y; }
        }
        /// <summary>
        /// ボタンや方向キーに触れているか
        /// </summary>
        public bool IsButtonAndAxisIsNoZero
        {
            get
            {
                return (this.isFire1 || this.isFire2 || this.isFire3 || (this.Horizontal != 0.0f) || (this.Virtical != 0.0f));
            }
        }
        
        // Use this for initialization
        protected override void Start()
        {
            base.Start();
            if (instance != this)
            {
                return;
            }

            for (int i = 0; i < this.joy_buttons_buf.Length; i++)
                this.joy_buttons_buf[i] = false;
            this.joy_Axis_buf = new Vector2();
            this.isInputAllow = true;
            this.isPlayString = false;
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (this._isInputWaiting)
            {
                if (this.axisdelay > 0)
                    this.axisdelay--;
                if (this.delay > 0)
                {
                    this.delay--;
                    return;
                }
                this.CheckWaitingAndRun();
                return;
            }

            if (this.IsAxisZero())
            {
                this.isFirstAxis = true;
                this.axisdelay = 0;
                this.isRepeat = false;
            }


            if (this.axisdelay > 0)
                this.axisdelay--;
            if (this.delay > 0)
            {
                this.delay--;
                return;
            }

            this.ButtonInputSet();
            this.AxisInputSet();
        }

    }
}