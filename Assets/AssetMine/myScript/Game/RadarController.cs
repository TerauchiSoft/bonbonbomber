﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// レーダーの表示範囲の変更、敵の表示
/// </summary>
public class RadarController : MonoBehaviour
{
    public Vector2 originMinimapPos;    // プレイヤーが(0, 0)のときのミニマップ原点座標
    public RectTransform minimapRectTrs;
    public Transform zunkoTrs;
    public float span = 7.2f;

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos = originMinimapPos;
        pos = pos + new Vector2(span * -zunkoTrs.position.x, span * -zunkoTrs.position.z);
        minimapRectTrs.anchoredPosition = pos;
    }

    /// <summary>
    /// ミニマップ初期化
    /// </summary>
    private void Init()
    {
        minimapRectTrs.anchoredPosition = originMinimapPos;
    }
}
