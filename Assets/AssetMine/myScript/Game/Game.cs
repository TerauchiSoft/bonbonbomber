﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System;

namespace TSSoft {
    /// <summary>
    /// ゲーム開始デモ->ゲーム開始スタート->ゲームの全体の値の管理 クラス
    /// </summary>
    public class Game : SingletonMonoBehaviour<Game>
    {
        public GameState gameState;
        private IDisposable initer;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();
            if (instance != this)
            {
                return;
            }

            // 初期化用メソッド
            this.initer?.Dispose();
            this.initer = Observable.EveryUpdate()
                .Select(_ => BootLoader.Instance.IsLoaded)  // 値の選択
                .DistinctUntilChanged() // 変化を検知
                //.Where(x => x)          // いらないと思う
                .Subscribe(val =>
                {
                    if (!BootLoader.Instance.IsLoaded)
                        return;

                    Debug.LogFormat("isloaded_{0}", BootLoader.Instance.IsLoaded.ToString());
                    if (BootLoader.Instance.IsLoaded)
                    {
                        this.Init();
                        this.initer.Dispose();
                        Debug.Log("INIT");
                    }
                })
                .AddTo(BootLoader.Instance.gameObject);
        }

        /// <summary>
        /// 初期化
        /// </summary>
        private void Init()
        {
            this.gameState = GameState.INTORO;
            FadeController.Instance.Fadein(1.0f);
        }
    }
}