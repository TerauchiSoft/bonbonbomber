﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TSSoft
{
    /// <summary>
    /// タイトル部のコントローラー
    /// </summary>
    public class TitleController : MonoBehaviour
    {
        public float waitDuration = 1.5f;
        public float delayDuration = 1.0f;
        public float moveDuration = 1.2f;
        public float upperMoveSpan = 4.0f;
        public float bottomMoveSpan = 4.0f;
        public Transform upperTrs;
        public Transform bottomTrs;

        private float tim;
        private const string FIRE1 = "Fire1";
        private bool isToFadeout = false;

        // Start is called before the first frame update
        private void Start()
        {
            tim = Time.time;
            FadeController.Instance.Fadein(waitDuration);
        }

        // Update is called once per frame
        void Update()
        {
            if (isToFadeout || tim + waitDuration > Time.time)
                return;

            if (Input.GetButton(FIRE1))
                ToNextScene();
        }

        /// <summary>
        /// 次のシーンへ
        /// </summary>
        private void ToNextScene()
        {
            FadeController.Instance.Fadeout(waitDuration, delayDuration, () => SceneChanger.Instance.GotoScene(SceneEnum.HOWTOPLAY));
            upperTrs.DOMoveY(upperTrs.position.y + upperMoveSpan, moveDuration).SetEase(Ease.InBounce);
            bottomTrs.DOMoveY(bottomTrs.position.y - bottomMoveSpan, moveDuration).SetEase(Ease.InBounce);
            isToFadeout = true;
        }
    }
}