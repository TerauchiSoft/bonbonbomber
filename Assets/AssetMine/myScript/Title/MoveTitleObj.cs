﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace TSSoft
{
    /// <summary>
    /// タイトルのオブジェクトを動かす
    /// </summary>
    public class MoveTitleObj : MonoBehaviour
    {
        public Transform[] moveObjs;
        public float moveSpan = 5f;
        public float duration = 1.0f;
        // Start is called before the first frame update
        void Start()
        {
            foreach (var o in moveObjs)
            {
                if (o != null)
                {
                    var nowRot = o.rotation;
                    var nowPos = o.position;
                    // 初期位置を動かす分上にずらす
                    o.transform.position = new Vector3(nowPos.x, nowPos.y + moveSpan, nowPos.z);
                    // 初期回転を動かす分逆にずらす
                    o.transform.rotation = new Quaternion();
                    // 初期位置にオブジェクトを動かす
                    var ye = nowPos.y;
                    o.DOMoveY(ye, duration).SetEase(Ease.OutBounce);
                    o.DORotate(nowRot.eulerAngles, duration).SetEase(Ease.OutBounce);
                }
            }
        }
    }
}