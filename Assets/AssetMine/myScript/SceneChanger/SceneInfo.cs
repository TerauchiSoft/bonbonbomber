﻿using UnityEngine.SceneManagement;

namespace TSSoft
{
    /// <summary>
    /// シーンマネージャーで管理するクラスの情報
    /// </summary>
    [System.Serializable]
    public class SceneInfo
    {
        public string sceneName;
        public SceneEnum sceneEnum;
        public float durationFadeIn;
        public float durationFadeOut;
    }
}
