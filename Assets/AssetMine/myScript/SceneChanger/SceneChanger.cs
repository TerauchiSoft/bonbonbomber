﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace TSSoft
{
    /// <summary>
    /// シーン遷移をコントロール
    /// </summary>
    public class SceneChanger : SingletonMonoBehaviour<SceneChanger>
    {
        public SceneInfo[] sceneInfo;
        public SceneEnum bootScene = SceneEnum.TITLE;
        private SceneInfo current;
        private Coroutine coroutine;
        private Action callBack;
        
        /// <summary>
        /// 初期化
        /// </summary>
        public void Init()
        {
            for (int i = 0; i < this.sceneInfo.Length; i++)
            {
                if (bootScene == this.sceneInfo[i].sceneEnum)
                {
                    GameObject.DontDestroyOnLoad(this);
                    current = this.sceneInfo[i];
                    this.GotoScene(bootScene);
                    return;
                }
            }
        }

        /// <summary>
        /// シーン移動
        /// </summary>
        /// <param name="sceneEnum"></param>
        /// <param name="callback"></param>
        public void GotoScene(SceneEnum sceneEnum, Action callback = null)
        {
            if (this.coroutine != null)
            {
                StopCoroutine(this.coroutine);
                this.coroutine = null;
            }
            this.callBack = callback;
            for (int i = 0; i < this.sceneInfo.Length; i++)
            {
                if (sceneEnum == this.sceneInfo[i].sceneEnum)
                {
                    this.current = this.sceneInfo[i];
                    this.coroutine = this.StartCoroutine(this.GotoSceneCo());
                    return;
                }
            }
        }

        /// <summary>
        /// シーン移動コルーチン
        /// </summary>
        /// <returns></returns>
        private IEnumerator GotoSceneCo()
        {
            yield return new WaitForSeconds(this.current.durationFadeOut);
            yield return SceneManager.LoadSceneAsync(this.current.sceneName, LoadSceneMode.Single);
            if (this.callBack != null)
                this.callBack.Invoke();
            yield return new WaitForSeconds(this.current.durationFadeIn);
        }

        /// <summary>
        /// 現在のシーンを取得
        /// </summary>
        /// <returns></returns>
        public SceneInfo GetCurrent()
        {
            return this.current;
        }
    }
}